// See https://aka.ms/new-console-template for more information
namespace Week4ConditionalStataments 
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Variable types.
            int x = 5;
            int y = 10;
            int resultAdding = x + y;
            int resultMultiplying = x * y;

            double resultValue = resultAdding + y;

            // Create a variable to read from user input.
            Console.WriteLine("Well, hello!");
            string input = Console.ReadLine();
            bool didTheySayHello = input == "hello";
            bool didTheySayHi = input == "hi";
            string helloValue = "Hello";
            bool didTheySaySomethingElse = (input != "hi");

            //Console.WriteLine(Console);

            // if (input == "hi")
            // Logical AND: said hi and hello (ambigious, and would never happen)
            //if (didTheySayHi && didTheySayHello) 
            // Logical NOT: didn't say hi AND hello
            //if (!didTheySayHi && !didTheySayHello)
            // Logical AND: said hi and hello (ambigious, and would never happen)
            if (didTheySayHi || didTheySayHello) 
            {
                // 1.
                Console.WriteLine("Oh hi there.");
            }
            else if (input == "hello")
            {
                // 2.
                Console.WriteLine(input);
                Console.WriteLine(resultValue);   
            }
            else
            {
                // 3.
                Console.WriteLine("Well, good afternoon to you too!");
            }

            // Switch example.
            switch (input)
            {
                case "hello":
                    Console.WriteLine("Well, hello");
                    // Add code inside each statement.
                    if (true)
                    {
                        Console.WriteLine(input);
                    }
                    break;
                case "hi":
                    Console.WriteLine("Well, hi");
                    break;
                default:
                    Console.WriteLine("Oh, well!");
                    break;
            }

            // Loop: While example.
            int index = 0;
            while (index < 5)
            {
                // @todo: To do in the future.
                Console.WriteLine(index);
                index++;
            }
        }
    }

}
